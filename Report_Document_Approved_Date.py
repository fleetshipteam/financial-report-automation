# -*- coding: utf-8 -*-
"""
Created on Thu Jul 29 15:30:26 2021

@author: kunal.s.chauhan
"""

# This file contains the working for code of Report with parameter Document Approved Date


import pandas as pd
import numpy as np
import math
import win32com.client as client
import os
from pywintypes import com_error
from pathlib import Path
import sys
import os.path
from os import path

win32c=client.constants
outlook=client.Dispatch("Outlook.Application")
excel=client.Dispatch('Excel.Application')
excel = client.gencache.EnsureDispatch('Excel.Application')

# func1 has all the working for the entire file
def func1():
    # importing required excel files

    excel_file = pd.ExcelFile("ES_Data_Document_Approved_Date.xlsx")
    df=excel_file.parse(sheet_name = "Document approved-Data")
    
    # Converting all neccesary dates to datetime-format
    df['Invoice date']=pd.to_datetime(df['Invoice date'], unit='s')
    df['Approval start date']=pd.to_datetime(df['Approval start date'], unit='s')
    df['Final approval date']=pd.to_datetime(df['Final approval date'], unit='s')
    df['Document approved date']=pd.to_datetime(df['Document approved date'], unit='s')
    
    # checking if any Document Approved Date Missing
    if df['Document approved date'].isna().sum()>0:
        df2=df[df['Document approved date'].isna()==True].copy()
        with pd.ExcelWriter('Report_Document_approved_date_Outlier_Document_approved_date_Missing.xlsx') as writer:
            df2.to_excel(writer, sheet_name='Sheet 1')
    
    #Continuing code if no errors
    else:
        #reading files from master data
        excel=pd.ExcelFile("Master Data.xlsx")
        df6=excel.parse(sheet_name="Sheet1")
        df5=excel.parse(sheet_name ="VesselList")
        df4=excel.parse(sheet_name ="VendorLog")
        
        df1=df6[['Currency','Exchange Rate']].copy()
        df2=df6[['Company Code','Company Name','Country']].copy()
        df3=df6[['Department code','Department Name','Department Type']].copy()
        
        # Adding column for Approval Month
        for j in range(len(df['Document approved date'])):
            if df.at[j,'Document approved date'].month==1:
                df.at[j,'Approval_Month']='January'
            elif df.at[j,'Document approved date'].month==2:
                df.at[j,'Approval_Month']='February'
            elif df.at[j,'Document approved date'].month==3:
                df.at[j,'Approval_Month']='March'
            elif df.at[j,'Document approved date'].month==4:
                df.at[j,'Approval_Month']='April'
            elif df.at[j,'Document approved date'].month==5:
                df.at[j,'Approval_Month']='May'
            elif df.at[j,'Document approved date'].month==6:
                df.at[j,'Approval_Month']='June'
            elif df.at[j,'Document approved date'].month==7:
                df.at[j,'Approval_Month']='July'
            elif df.at[j,'Document approved date'].month==8:
                df.at[j,'Approval_Month']='August'
            elif df.at[j,'Document approved date'].month==9:
                df.at[j,'Approval_Month']='September'
            elif df.at[j,'Document approved date'].month==10:
                df.at[j,'Approval_Month']='October'
            elif df.at[j,'Document approved date'].month==11:
                df.at[j,'Approval_Month']='November'
            elif df.at[j,'Document approved date'].month==12:
                df.at[j,'Approval_Month']='December'
        
        # Adding column for Approval Year
        a,b=df.shape
        df.insert(b,"Approval Year",'0')
        for i in range(len(df['Document approved date'])):
            df.at[i,'Approval Year']=math.trunc(df.at[i,'Document approved date'].year)
        
        # Adding column for Invoice Year
        a,b=df.shape
        df.insert(b,"Invoice Year",'0')
        for i in range(len(df['Invoice date'])):
            df.at[i,'Invoice Year']=math.trunc(df.at[i,'Invoice date'].year)
        
        # Adding columns for 2 stages of Approval Days for Report 2
        df["Approval Time: Stage 1"]=df['Final approval date']-df['Approval start date']
        df["Approval Time: Stage 2"]=df['Document approved date']-df['Final approval date']
        df["Approval Days: Stage 1"]=(df["Approval Time: Stage 1"].dt.days)+1
        df["Approval Days: Stage 2"]=(df["Approval Time: Stage 2"].dt.days)+1
        df["Approval Days: Combined"]=df["Approval Days: Stage 1"]+df["Approval Days: Stage 2"]
        
        # Correcting Vessel code
        df7=df6[['Original Vessel ID','New Vessel ID']].copy()
        df7.dropna(subset=['Original Vessel ID'],inplace=True)
        for i in df.index:
            for j in df7.index:
                if df.at[i,'Vessel code']==df7.at[j,'Original Vessel ID']:
                    df.at[i,'Vessel code']=df7.at[j,'New Vessel ID']
        
        # handling data in df1(Currency Data),df2(Company and Location Data),df3(Department Data)
        df1.dropna(subset=["Currency"],inplace=True)
        df1=df1.reset_index()
        df1.drop(labels=['index'],axis=1,inplace=True)
        df2.dropna(subset=['Company Name'],inplace=True)
        df2=df2.reset_index()
        df2.drop(labels=['index'],axis=1,inplace=True)
        df3.dropna(subset=['Department code'],inplace=True)
        df3=df3.reset_index()
        df3.drop(labels=['index'],axis=1,inplace=True)
        
        #Changing int department codes to string
        for i in df3.index:
            if type(df3.at[i,'Department code'])==int:
                df3.at[i,'Department code']=str(df3.at[i,'Department code'])
        
        # importing 'Exchange Rate' and getting 'USD Value' column using exchange rate file
        df=pd.merge(df,df1,on=['Currency'],how='left')
        df['USD Value']=df['Gross amount']/df['Exchange Rate']
        df['USD Value']=round(df['USD Value'],2)
        
        # getting columns for 'Company Code' and 'Country' 
        df=pd.merge(df,df2,left_on=['Company'],right_on=['Company Name'],how='left')
        
        # getting columns for 'Department Name' and 'Department Type'
        df=pd.merge(df,df3,left_on=['Department'],right_on=['Department code'],how='left')
        
        
        # getting column for 'REQUESTING DEPT.' using df4
        df4=df4[['VENDOR NUMBER','REQUESTING DEPT.']].copy()
        df4.drop_duplicates(subset='VENDOR NUMBER', keep="first", inplace= True)
        df4=df4.reset_index()
        df=pd.merge(df,df4,left_on=['Supplier id'],right_on=['VENDOR NUMBER'],how='left')
        
        #Checking and correcting Vessel code line
        if df['Vessel code line'].isna().sum()>0:
            df2=df[df['Vessel code line'].isna()==True]
            a=df2.index
            for i in a:
                df.at[i,'Vessel code line']=0
        
        # getting empty vessle codes using vessle code line
        for i in df.index:
            a=df.at[i,'Vessel code line']
            if type(a)!=float and type(a)!=int and type(a)!=np.float64 and type(a)!=np.int64:
                df.at[i,'Vessel Code 2']=a[0:4]
            else:
                df.at[i,'Vessel Code 2']=a
        for i in df.index:
            if df.at[i,'Vessel code']==0:
                df.at[i,'Vessel code']=df.at[i,'Vessel Code 2']
        
        # getting column for 'New Department Grouping' which gives vessel type, using df5; also here '4 codes' corresponds to the 'Vessel code'
        df5=df5[['4 Codes','New Department Grouping']].copy()
        df5.dropna(subset=['4 Codes'],inplace=True)
        df5=df5.reset_index()
        df5.drop(labels=['index'],axis=1,inplace=True)
        df=pd.merge(df,df5,left_on=['Vessel code'],right_on=['4 Codes'],how='left')
        
        # defining technical and non-technical for vessles
        for i in range(len(df['Department Type'])):
            if df.at[i,'Department Type']=='NTECH':
                df.at[i,'New Department Grouping']='Non-Technical'
        # if no vessle number given defining as 'Non-Technical'
        for i in df.index:
            if df.at[i,'Vessel code']==0:
                df.at[i,'New Department Grouping']='Non-Technical'
            if np.isnan(df.at[i,'Vessel code']):
                df.at[i,'New Department Grouping']='Non-Technical'
        
        df.drop(labels=["Approval Time: Stage 1","Approval Time: Stage 2",'Company Name','Department code','VENDOR NUMBER','4 Codes'],axis=1,inplace=True)
        
        # Now we work on droppinf repeating invoices
        # First we will figure out which vendors have repeating invoices (it is not uncommon for 2 different vendors to have the same invoice number, therefore we will only drop invoices if they come from the same vendor, as specified by the finance department)
        # For this we will use multiple versions of the groupby function 
        df2=df[['Supplier id','Serial number']].copy()
        df3=df2.groupby(by=['Supplier id']).nunique()
        df4=df2.groupby(by=['Supplier id']).count()
        df4['diff']=df4['Serial number']-df3['Serial number']
        df5=df4[df4['diff']!=0]
        # df5 now has all vendors who have repeating invoices
        
        # now running a for loop to drop repeating invoices where each loop iterates on the supplier id, all duplicate indexes are found and then dropped
        for i in df5.index:
            df6=df[df['Supplier id']==i]
            df8=df6[df6.duplicated(subset=['Serial number'],keep='first')==True]
            index_names=df8.index
            df.drop(index_names,inplace=True)
        
        # dropping all invoices where 2nd stage of approval is not cmplete
        df=df[pd.notna(df['Approval Days: Stage 2'])]
        
        # Resetting index and making sure the dataframe's index has the name index; in case this is false, the pivot table code fails, therefore this step is crucial and should not be removed
        df=df.reset_index()
        df.drop(labels=['index'],axis=1,inplace=True)
        df=df.reset_index()
        df=df.set_index('index')
        
        # Checking if Input File exists. If it does, adding it to the current dataframe if it does exist
        if path.exists('Report_DocumentApprovedDate_Input.xlsx')==True:
            excel2=pd.ExcelFile('Report_DocumentApprovedDate_Input.xlsx')
            df7=excel2.parse(sheet_name='Sheet 1')
            df=df.append(df7,ignore_index=True)
            df.drop(labels=['index'],axis=1,inplace=True)
            
            # Now within this condition checking if invoices repeat and if they do showing it as outlier
            df2=df[['Supplier id','Serial number']].copy()
            df3=df2.groupby(by=['Supplier id']).nunique()
            df4=df2.groupby(by=['Supplier id']).count()
            df4['diff']=df4['Serial number']-df3['Serial number']
            df5=df4[df4['diff']!=0]
            df2=pd.DataFrame(columns=df.columns)
            index_names=[]
            for i in df5.index:
                df6=df[df['Supplier id']==i]
                df8=df6[df6.duplicated(subset=['Serial number'])==True]
                index_names.append(df8.index)
            for i in range(len(index_names)):
                j=index_names[i]
                df5=df.loc[j]
                df2=df2.append(df5,ignore_index=True)
            if df2.empty==False:
                with pd.ExcelWriter('Report_DocumentApprovedDate_Outlier_RepeatOverMonths.xlsx') as writer:
                        df2.to_excel(writer, sheet_name='Sheet 1')
        
        # Resetting index and making sure the dataframe's index has the name index; in case this is false, the pivot table code fails, therefore this step is crucial and should not be removed
        df=df.reset_index()
        df=df.set_index('index')
        
        # Saving the datframe in excel with the name Report2
        with pd.ExcelWriter('Report_DocumentApprovedDate_Output.xlsx') as writer:
            df.to_excel(writer, sheet_name='Sheet 1')
        with pd.ExcelWriter('Report_DocumentApprovedDate_Input.xlsx') as writer:
            df.to_excel(writer, sheet_name='Sheet 1')
        # working of report 2 is completed


# now we stat work on creating the pivot tables
def pivot_table(wb: object, ws1: object, pt_ws: object, ws_name: str, pt_name: str, pt_rows: list, pt_cols: list, pt_filters: list, pt_fields: list, title: str, comment: str, row_label: str, col_label: str):
    """
    wb = workbook1 reference
    ws1 = worksheet1
    pt_ws = pivot table worksheet number
    ws_name = pivot table worksheet name
    pt_name = name given to pivot table
    pt_rows, pt_cols, pt_filters, pt_fields: values selected for filling the pivot tables
    """

    # pivot table location
    pt_loc = len(pt_filters) + 5 #if you need more comments increase the value here
    
    # grab the pivot table source data
    pc = wb.PivotCaches().Create(SourceType=win32c.xlDatabase, SourceData=ws1.UsedRange)
    
    # create the pivot table object
    pc.CreatePivotTable(TableDestination=f'{ws_name}!R{pt_loc}C1', TableName=pt_name)

    # selecte the pivot table work sheet and location to create the pivot table
    pt_ws.Select()
    pt_ws.Cells(pt_loc, 1).Select()
    
    # setting format for worksheet
    pt_ws.Cells.Font.Name = "Arial"
    pt_ws.Cells.Font.Size = 9
    
    # pivot table title and comments
    pt_ws.Cells(1,1).Value = title # main title
    pt_ws.Cells(1,1).Font.Size = 12
    pt_ws.Cells(1,1).Font.Bold = True
    pt_ws.Cells(2,1).Value = comment # title which explain following pivot
    pt_ws.Cells(2,1).Font.Size = 10
    
    # Sets the rows, columns and filters of the pivot table
    for field_list, field_r in ((pt_filters, win32c.xlPageField), (pt_rows, win32c.xlRowField), (pt_cols, win32c.xlColumnField)):
        for i, value in enumerate(field_list):
            pt_ws.PivotTables(pt_name).PivotFields(value).Orientation = field_r
            pt_ws.PivotTables(pt_name).PivotFields(value).Position = i + 1
    
    # Sets the Values of the pivot table
    for field in pt_fields:
        pt_ws.PivotTables(pt_name).AddDataField(pt_ws.PivotTables(pt_name).PivotFields(field[0]), field[1], field[2]).NumberFormat = field[3]
    
    # Adding Row and Column Labels
    x=len(pt_cols)
    y=len(pt_fields)
    if y>1:
        z=2
    else:
        z=1
    pt_ws.Cells(pt_loc+x+z-1,1).Value = row_label
    pt_ws.Cells(pt_loc,2).Value = col_label
    
    # Using Pivot Table Functions to improve the Pivot Table
    pt_ws.PivotTables(pt_name).ShowValuesRow = True
    pt_ws.PivotTables(pt_name).ColumnGrand = True 
    pt_ws.PivotTables(pt_name).ShowTableStyleRowHeaders = True
    pt_ws.PivotTables(pt_name).ShowTableStyleColumnHeaders = True
    pt_ws.PivotTables(pt_name).RowGrand = True 
    pt_ws.PivotTables(pt_name).PrintTitles = True
    pt_ws.PivotTables(pt_name).TableStyle2 = "PivotStyleLight15"

#this function will be used to run the excel files
def run_excel(f_path: Path, f_name: str, sheet_name: str, new_sheet_name: str, pt_name: str, pt_rows: list, pt_cols: list, pt_filters: list, pt_fields: list,title: str, comment: str, row_label: str, col_label: str):

    filename = f_path / f_name

    # create excel object
    excel = client.gencache.EnsureDispatch('Excel.Application')

    # excel can be visible or not
    excel.Visible = True  # False
    
    # try except for file / path
    try:
        wb = excel.Workbooks.Open(filename)
    except com_error as e:
        if e.excepinfo[5] == -2146827284:
            print(f'Failed to open spreadsheet.  Invalid filename or location: {filename}')
        else:
            raise e
        sys.exit(1)

    # set worksheet
    ws1 = wb.Sheets(sheet_name)
    
    # Setup and call pivot_table
    ws2_name = new_sheet_name
    wb.Sheets.Add().Name = ws2_name
    ws2 = wb.Sheets(ws2_name)
    
    
    # Calling Pivot Table function
    pivot_table(wb, ws1, ws2, ws2_name, pt_name, pt_rows, pt_cols, pt_filters, pt_fields, title, comment, row_label, col_label)
    
    #save the excel file
    wb.Save()

# pivot table for report 2
def pt(f_path: Path, f_name: str, sheet_name: str):
    
    #Parameters for each iteration
    name_of_new_sheet='SheetPT1'  #need to change in every iteration
    pt_name = 'Table01'  # must be a string
    pt_rows = ['Department Name']  # must be a list
    pt_cols = ['Approval Year','Approval_Month']  # must be a list
    pt_filters = ['Company','Country','Invoice priority','Approval Year','Approval_Month','New Department Grouping']  # must be a list
    # [0]: field name [1]: pivot table column name [3]: calulation method [4]: number format
    pt_fields = [['Serial number', 'No. of Invoice', win32c.xlCount, '0'],
                 ['Approval Days: Stage 1', 'Avg Days Stage 1 Approval', win32c.xlAverage, '#,##0.0'],
                ['Approval Days: Stage 2', 'Avg Days Stage 2 Approval', win32c.xlAverage, '#,##0.0'],
                ["Approval Days: Combined",'Avg Days Combined Approval', win32c.xlAverage, '#,##0.0']] # must be list of lists
    Title  = 'Fleet Group of Companies' # must be string
    Comment = 'Average Days to Approve Invoices-Filtered on Priority' # must be string
    row_label = 'Department' # must be string
    column_label = 'Approval Month' # must be string
    run_excel(f_path,f_name, sheet_name,name_of_new_sheet, pt_name, pt_rows, pt_cols, pt_filters, pt_fields, Title, Comment, row_label, column_label)

# defining the file path, name and sheet name
def pivot():
    # changing file path and name
    f_path=Path.cwd()
    f_name='Report_DocumentApprovedDate_Output.xlsx'
    sheet_name='Sheet 1'
    
    # calling other functions
    pt(f_path, f_name, sheet_name)

# calling functions to complete code
func1()
if os.path.exists('Report_DocumentApprovedDate_Output.xlsx')==True:
    pivot()
