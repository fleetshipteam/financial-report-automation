# -*- coding: utf-8 -*-
"""
Created on Mon Aug 16 12:16:14 2021

@author: kunal.s.chauhan
"""

import win32com.client as client
import win32api
from win32com.client import Dispatch
import os
import pandas as pd
import numpy as np
import math
import pathlib
from pywintypes import com_error
from pathlib import Path
import sys
win32c=client.constants
outlook=client.Dispatch("Outlook.Application")
excel = client.gencache.EnsureDispatch('Excel.Application')

def email(month:str, to:str, name:str, link:str):
    message=outlook.CreateItem(0)
    message.Display()
    message.To=to
    message.CC='vineetrathi@fleetship.com; salima.martrust@fleetship.com; nachiket.martrust@fleetship.com'
#     message.CC='alan.lo@fleetship.com'
    message.Attachments.Add(link)
    message.Subject="Eye Share- Rejected Invoices-Month of "+month
    html_body= """
        <div>
            </b>Dear {},</b>
        </div><br>
        <div>
            Attaching the list of invoices where the invoice is rejected without mandatory tags/ has incorrect tag updated in Eye share. Please note tag update is critical and ensure this is not missed at the time of rejection of an invoice in future.
        </div><br>
        <div>
            Thanks and regards<br>
            Accounts Payable<br>
            Fleet Management Limited<br>
        </div>
        """
    message.HTMLBody=html_body
    message.HTMLBody=html_body.format(name)
    message.Display()
    message.Send()

def working():
    
    # reading required excel files
    xlsx_RejDate=pd.ExcelFile("ES_Data_Rejected_Date.xlsx")
    df_RejDate=xlsx_RejDate.parse(sheet_name='Invoice')
    xlsx_mst=pd.ExcelFile("Master Data.xlsx")
    df_dimtables=xlsx_mst.parse(sheet_name='Sheet1')
    
    df_RejDate['Rejected date']=pd.to_datetime(df_RejDate['Rejected date'], unit='s')    
    # Checking if any rejected date is missing
    if df_RejDate['Rejected date'].isna().sum()>0:
        df_RejDateNA = df_RejDate[df_RejDate['Rejected date'].isna()]
        with pd.ExcelWriter('Report_Rejected_Date_Outlier_Rejected_Date_Missing.xlsx') as writer:
            df_RejDateNA.to_excel(writer, sheet_name='Sheet 1')
    
    # continuing code if all rejected dates exist
    else:
        
        #Adding column for year and month 
        df_RejDate["Rejected year"] = df_RejDate["Rejected date"].dt.year
        df_RejDate["Rejected month"] = df_RejDate["Rejected date"].dt.month_name()
        
        #Now cleansing and merging columns from Master Data to Rej Date table
        df_company = df_dimtables[['Company Code','Company Name','Country']]
        df_accountant = df_dimtables[['Accountant','Accountant email id']]
        df_dept = df_dimtables[['Department code', 'Department Name']]

        df_company = df_company.dropna(subset=["Company Name"])
        df_accountant = df_accountant.dropna(subset=["Accountant"])
        df_dept = df_dept.dropna(subset=["Department code"])
        df_dept["Department code"] = df_dept["Department code"].astype(str)
        
        df_RejDate = df_RejDate.merge(df_company, left_on=['Company'], right_on=['Company Name'], how='left') \
            .merge(df_accountant, left_on=['Rejected name'], right_on=['Accountant'], how='left') \
            .merge(df_dept, left_on=['Department'], right_on=['Department code'], how='left')
        
        #Checking if all accountant email id are added, if not giving outlier
        if df_RejDate['Accountant email id'].isna().sum()>0:
            df_EMailNA = df_RejDate[df_RejDate["Accountant email id"].isna()] \
                .filter(items=['Rejected name','Invoice no']) \
                .groupby(by=['Rejected name']).count()
            with pd.ExcelWriter('Report_Rejected_Date_Outlier_Email_Missing.xlsx') as writer:
                df_EMailNA.to_excel(writer, sheet_name='Sheet 1')
        
        #Continuing code if all accountant emails exist
        else:
            
            #Now we will check if there are any outlier on the basis of Tags
            
            #First we check for empty tags
            df_tagsNA = df_RejDate[df_RejDate.Tags.isna()]
            df_RejDate.dropna(subset=['Tags'], inplace=True)
            
            #Then, we check for non-manual Tags
            df_tagsNA = df_tagsNA.append(df_RejDate[~df_RejDate.Tags.str.contains('#Manual')])
            df_RejDate = df_RejDate[df_RejDate.Tags.str.contains('#Manual')]
            
            #Now, to see which tags are allowed
            tags_allowed = df_dimtables.Tags.dropna().values

            #Now, to check whether any tags allowed for each tags
            #Now we keep the tags after "#Manual:" and what tags are allowed only
            df_RejDate['TagsList'] = df_RejDate.Tags.str.extract(r'#Manual:(.*)[^#]?$')[0] \
                .fillna('') \
                .str.replace('Duplicate invoice', 'Invoices-Duplicate') \
                .str.replace('For Cyprus Office', 'Invoices-Branch Office Cyprus') \
                .str.split(',').apply(lambda x: list(set([x_sub.strip() for x_sub in x if x_sub.strip() in tags_allowed])))
            df_tagsNA['TagsList'] = df_tagsNA.Tags.str.extract(r'#Manual:(.*)[^#]?$')[0] \
                .fillna('') \
                .str.replace('Duplicate invoice', 'Invoices-Duplicate') \
                .str.replace('For Cyprus Office', 'Invoices-Branch Office Cyprus') \
                .str.split(',').apply(lambda x: list(set([x_sub.strip() for x_sub in x if x_sub.strip() in tags_allowed])))
            
            #Move all the rows if len(Tags) != 1 to df_tagsNA
            df_tagsNA = df_tagsNA.append(df_RejDate[df_RejDate.TagsList.apply(lambda x: len(x)) < 1])

            
            #Now all factors for outlier have been considered, so, if df2 is empty output should be given if not outlier should be emailed and corrected

            #First taking input of last month, if it exists
            if df_tagsNA.empty:
                if os.path.exists('Report_Rejected_Date_Input.xlsx'):
                    xlsx_RejDateInput = pd.ExcelFile('Report_Rejected_Date_Input.xlsx')
                    df_RejDateInput = xlsx_RejDateInput.parse(sheet_name='Sheet 1')
                    df_RejDate = df_RejDate.append(df_RejDateInput, ignore_index=True)
                    df_RejDate.drop(labels=['index'], axis=1, inplace=True)

                    # Take out all invoices with suppliers who have repeated serial nos.
                    n_SN = df_RejDate[["Supplier id", "Serial number"]].value_counts()
                    suppliers_repSN = n_SN[n_SN > 1].index.get_level_values('Supplier id')[0]
                    df_repOverMths = df_RejDate[df_RejDate['Supplier id'].isin(suppliers_repSN)]

                    if not df_repOverMths.empty:
                        with pd.ExcelWriter('Report_Rejected_Date_Outlier_RepeatOverMonths.xlsx') as writer:
                                df_repOverMths.to_excel(writer, sheet_name='Sheet 1')

            #Now we will give output and input if df2 is empty
            if df_tagsNA.empty:
                df_RejDate = df_RejDate.reset_index()
                df_RejDate = df_RejDate.set_index('index')
                df_RejDate.loc[df_RejDate['Tags'].isin(['nan', '']), 'Tags'] = 'Tag Missing'
                df_RejDate.loc[~df_RejDate['Tags'].isin(['nan', '']), 'Tags'] = df_RejDate.loc[~df_RejDate['Tags'].isin(['nan', '']), 'TagsList'].apply(lambda x: x[0] if x else None)                
                with pd.ExcelWriter('Report_Rejected_Date_Output.xlsx') as writer:
                    df_RejDate.to_excel(writer, sheet_name='Sheet 1')
                with pd.ExcelWriter('Report_Rejected_Date_Input.xlsx') as writer:
                    df_RejDate.to_excel(writer, sheet_name='Sheet 1')
                       
            # if df2 is not empty, we will first reformat the excel
            if not df_tagsNA.empty:
                df_tagsNA['Tags'] = df_tagsNA['Tags'].fillna('nan')
                is_tagsNA = df_tagsNA['Tags'].str.strip().isin(['nan', ''])
                df_tagsNA.loc[is_tagsNA, 'Tags'] = 'Tag Missing'
                # For outliers, keep the original message.
                df_tagsNA.loc[~is_tagsNA, 'Tags'] = df_tagsNA.loc[~is_tagsNA, 'Tags'].str.extract(r'#Manual:(.*)[^#]?$')[0]
                df_tagsNA_out = df_tagsNA[[
                    'Company', 'Department', 'Vessel code', 'Vessel', 'Supplier id'
                    , 'Supplier', 'Gross amount', 'Currency', 'Invoice no', 'Tags'
                    , 'Scan no', 'Registered date', 'Rejected date', 'Rejected name', 'Accountant email id'
                    , 'Last comment', 'Approval started by', 'Approval start date', 'Rejected year', 'Rejected month'
                ]].copy()

                #Then we will send an email to all the Accountants who put incorrect tag
                acct_outliers = df_tagsNA_out['Rejected name'].unique()
                the_month = df_tagsNA_out['Rejected month'].unique()[0]

                #We will ask the user weather they want to send an email or not
                question=input("Do you want to create an email for the outliers:(y/n):")

                if question=='y':
                    for acct in acct_outliers:
                        df_sub_tagsNA_out = df_tagsNA_out[df_tagsNA_out['Rejected name'] == acct]
                        acct_email = df_sub_tagsNA_out['Accountant email id'].unique()[0]
                        attachment_name = f"Rejected_Report_Outlier_{acct}.xlsx"
                        with pd.ExcelWriter(attachment_name) as writer:
                            df_sub_tagsNA_out.to_excel(writer, sheet_name='Sheet 1')
                        excel_absolute=str(pathlib.Path(attachment_name).absolute())
                        email(the_month, acct_email, acct, excel_absolute)
#                         email(the_month, 'alan.lo@fleetship.com', acct, excel_absolute)

                if question=='n':
                    for acct in acct_outliers:
                        df_sub_tagsNA_out = df_tagsNA_out[df_tagsNA_out['Rejected name'] == acct]
                        acct_email = df_sub_tagsNA_out['Accountant email id'].unique()[0]
                        attachment_name = f"Rejected_Report_Outlier_{acct}.xlsx"
                        with pd.ExcelWriter(attachment_name) as writer:
                            df_sub_tagsNA_out.to_excel(writer, sheet_name='Sheet 1')                  
    
def pivot_table(wb: object, ws1: object, pt_ws: object, ws_name: str, pt_name: str, pt_rows: list, pt_cols: list, pt_filters: list, pt_fields: list, title: str, comment: str, row_label: str, col_label: str):
    """
    wb = workbook1 reference
    ws1 = worksheet1
    pt_ws = pivot table worksheet number
    ws_name = pivot table worksheet name
    pt_name = name given to pivot table
    pt_rows, pt_cols, pt_filters, pt_fields: values selected for filling the pivot tables
    """

    # pivot table location
    pt_loc = len(pt_filters) + 5 #if you need more comments increase the value here
    
    # grab the pivot table source data
    pc = wb.PivotCaches().Create(SourceType=win32c.xlDatabase, SourceData=ws1.UsedRange)
    
    # create the pivot table object
    pc.CreatePivotTable(TableDestination=f'{ws_name}!R{pt_loc}C1', TableName=pt_name)

    # selecte the pivot table work sheet and location to create the pivot table
    pt_ws.Select()
    pt_ws.Cells(pt_loc, 1).Select()
    
    # setting format for worksheet
    pt_ws.Cells.Font.Name = "Arial"
    pt_ws.Cells.Font.Size = 9
    pt_ws.Cells.BorderAround(win32c.xlThick)
    
    # pivot table title and comments
    pt_ws.Cells(1,1).Value = title # main title
    pt_ws.Cells(1,1).Font.Size = 12
    pt_ws.Cells(1,1).Font.Bold = True
    pt_ws.Cells(2,1).Value = comment # title which explain following pivot
    pt_ws.Cells(2,1).Font.Size = 10
    
    # Sets the rows, columns and filters of the pivot table
    for field_list, field_r in ((pt_filters, win32c.xlPageField), (pt_rows, win32c.xlRowField), (pt_cols, win32c.xlColumnField)):
        for i, value in enumerate(field_list):
            pt_ws.PivotTables(pt_name).PivotFields(value).Orientation = field_r
            pt_ws.PivotTables(pt_name).PivotFields(value).Position = i + 1

    # Sets the Values of the pivot table
    for field in pt_fields:
        pt_ws.PivotTables(pt_name).AddDataField(pt_ws.PivotTables(pt_name).PivotFields(field[0]), field[1], field[2]).NumberFormat = field[3]
    
    # Adding Row and Column Labels
    x=len(pt_cols)
    y=len(pt_fields)
    if y>1:
        z=2
    else:
        z=1
    pt_ws.Cells(pt_loc+x+z-1,1).Value = row_label
    pt_ws.Cells(pt_loc,2).Value = col_label
    
    # Visiblity True or False
    pt_ws.PivotTables(pt_name).ShowValuesRow = True
    pt_ws.PivotTables(pt_name).ColumnGrand = True 
    pt_ws.PivotTables(pt_name).ShowTableStyleRowHeaders = True
    pt_ws.PivotTables(pt_name).ShowTableStyleColumnHeaders = True
    pt_ws.PivotTables(pt_name).RowGrand = True 
    pt_ws.PivotTables(pt_name).PrintTitles = True
    pt_ws.PivotTables(pt_name).TableStyle2 = "PivotStyleLight15"
    
def run_excel(f_path: Path, f_name: str, sheet_name: str, new_sheet_name: str, pt_name: str, pt_rows: list, pt_cols: list, pt_filters: list, pt_fields: list,title: str, comment: str, row_label: str, col_label: str):

    filename = f_path / f_name

    # create excel object
    excel = client.gencache.EnsureDispatch('Excel.Application')

    # excel can be visible or not
    excel.Visible = True  # False
    
    # try except for file / path
    try:
        wb = excel.Workbooks.Open(filename)
    except com_error as e:
        if e.excepinfo[5] == -2146827284:
            print(f'Failed to open spreadsheet.  Invalid filename or location: {filename}')
        else:
            raise e
        sys.exit(1)

    # set worksheet
    ws1 = wb.Sheets(sheet_name)
    
    # Setup and call pivot_table
    ws2_name = new_sheet_name
    wb.Sheets.Add().Name = ws2_name
    ws2 = wb.Sheets(ws2_name)
   
    # Calling Pivot Table function
    pivot_table(wb, ws1, ws2, ws2_name, pt_name, pt_rows, pt_cols, pt_filters, pt_fields, title, comment, row_label, col_label)
    
    #save the excel file
    wb.Save()
    
def pt1(f_path: Path, f_name: str, sheet_name: str):
    
    #Parameters for each iteration
    name_of_new_sheet='SheetPT1'  #need to change in every iteration
    pt_name = 'Table01'  # must be a string
    pt_rows = ['Tags']  # must be a list
    pt_cols = ['Rejected year','Rejected month','Company']  # must be a list
    pt_filters = ['Rejected year','Rejected month']  # must be a list
    # [0]: field name [1]: pivot table column name [3]: calulation method [4]: number format
    pt_fields = [ ['Invoice no', 'No of Invoices', win32c.xlCount, '0']]
    Title  = 'Fleet Group of Companies' # must be string
    Comment = 'Invoices Rejected in the month-Tags' # must be string
    row_label = 'Tag' # must be string
    column_label = 'Year' # must be string
    run_excel(f_path,f_name, sheet_name,name_of_new_sheet, pt_name, pt_rows, pt_cols, pt_filters, pt_fields, Title, Comment, row_label, column_label)

def pt2(f_path: Path, f_name: str, sheet_name: str):
    
    #Parameters for each iteration
    name_of_new_sheet='SheetPT2'  #need to change in every iteration
    pt_name = 'Table02'  # must be a string
    pt_rows = ['Rejected name','Department','Department Name']  # must be a list
    pt_cols = ['Rejected year','Rejected month']  # must be a list
    pt_filters = ['Rejected year','Rejected month','Tags','Company','Country']  # must be a list
    # [0]: field name [1]: pivot table column name [3]: calulation method [4]: number format
    pt_fields = [ ['Invoice no', 'No of Invoices', win32c.xlCount, '0']]
    Title  = 'Fleet Group of Companies' # must be string
    Comment = 'Invoices Rejected in the month- Rejected Name' # must be string
    row_label = 'Accountant' # must be string
    column_label = 'Year' # must be string
    run_excel(f_path,f_name, sheet_name,name_of_new_sheet, pt_name, pt_rows, pt_cols, pt_filters, pt_fields, Title, Comment, row_label, column_label)

def write():
    
    # changing file path and name
    f_path=Path.cwd()
    f_name='Report_Rejected_Date_Output.xlsx'
    sheet_name='Sheet 1'
    
    # calling other functions
    pt1(f_path, f_name, sheet_name)
    pt2(f_path, f_name, sheet_name)

working()
if os.path.exists('Report_Rejected_Date_Output.xlsx'):
    write()