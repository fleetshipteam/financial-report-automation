# -*- coding: utf-8 -*-
"""
Created on Wed Aug 25 11:46:25 2021

@author: kunal.s.chauhan
"""

import win32com.client as client
import win32api
from win32com.client import Dispatch
import os
import pandas as pd
import numpy as np
import math
import pathlib
from pywintypes import com_error
from pathlib import Path
import sys
from os import path
from datetime import date

win32c=client.constants
outlook=client.Dispatch("Outlook.Application")
excel=client.Dispatch('Excel.Application')
excel = client.gencache.EnsureDispatch('Excel.Application')

def email(to:str, d, name:str, link:str):
    message=outlook.CreateItem(0)
    message.Display()
    message.To=to
    message.CC='vineetrathi@fleetship.com; salima.martrust@fleetship.com; nachiket.martrust@fleetship.com'
    message.Attachments.Add(link)
    message.Subject="AP Invoice- Over Ageing as on "+str(d)
    html_body= """
        <div>
            </b>{},</b>
        </div><br>
        <div>
            Find attached the over-ageing invoices >60 days as on {}. <br>
            Note: This is a system generate email and need not be responded to.
        </div><br>
        <div>
            Thanks and regards<br>
            Accounts Payable<br>
            Fleet Management Limited<br>
        </div>
        """
    message.HTMLBody=html_body
    message.HTMLBody=html_body.format(name,d)
    message.Display()
    message.Send()

def working():
    # importing required excel files
    excel_file = pd.ExcelFile("ES_Data_Ageing.xlsx")
    df=excel_file.parse(sheet_name = "Invoice")
    
    # Converting all neccesary dates to datetime-format
    df['Approval start date']=pd.to_datetime(df['Approval start date'], unit='s')
    # checking if any Approval Start Date Missing
    if df['Approval start date'].isna().sum()>0:
        df2=df[df['Approval start date'].isna()==True].copy()
        with pd.ExcelWriter('Report_Ageing_Outlier_Approval_start_date_Missing.xlsx') as writer:
            df2.to_excel(writer, sheet_name='Sheet 1')
    
    # Continuing code if no errors
    else:
        
        #Checking for Outliers
        df1=pd.DataFrame(columns=df.columns)
        df1['Reason']=''
        df['Invoice date']=pd.to_datetime(df['Invoice date'], unit='s')
        df['Due date']=pd.to_datetime(df['Due date'], unit='s')
        df['Period']=pd.to_datetime(df['Period'], unit='s')
        df['Registered date']=pd.to_datetime(df['Registered date'], unit='s')
        
        df['diff']=df['Due date']-df['Invoice date']
        df['diff']=df['diff'].dt.days
        df2=df[df['diff']<0].copy()
        df.drop(labels=['diff'],axis=1,inplace=True)
        if not df2.empty:
            df2['Reason']='Due Date before Invoice Date'
            df2.drop(labels=['diff'],axis=1,inplace=True)
            df1=df1.append(df2,ignore_index=True)
        
        a=[]
        b=[]
        for i in df.index:
            if df.at[i,'Period']==df.at[i,'Invoice date']:
                a.append(i)
            else:
                b.append(i)
        if len(b)>0:
            df2=pd.DataFrame(columns=df.columns)
            for i in range(len(b)):
                j=b[i]
                df2.loc[i]=df.loc[j]
            df2['Reason']='Invoice Date not equal to Period'
            df1=df1.append(df2,ignore_index=True)
        
        df3=df[df['PO number'].isna()==True].copy()
        df4=df[df['PO number'].isna()==False].copy()
        b=[]
        for i in df4.index:
            c=df4.at[i,'PO number'].split('/')
            if c[0]=='000':
                b.append(i)
            elif c[0]=='0000':
                b.append(i)
        if len(b)>0:
            df2=pd.DataFrame(columns=df.columns)
            for i in range(len(b)):
                j=b[i]
                df2.loc[i]=df.loc[j]
            df2['Reason']='Incorrect PO Number'
            df1=df1.append(df2,ignore_index=True)
        
        df['diff']=df['Approval start date']-df['Registered date']
        df['diff']=df['diff'].dt.days
        df2=df[df['diff']>2].copy()
        df.drop(labels=['diff'],axis=1,inplace=True)
        if not df2.empty:
            df2['Reason']='Difference between Approval Start Date and Registered Date is more than 2'
            df2.drop(labels=['diff'],axis=1,inplace=True)
            df1=df1.append(df2,ignore_index=True)
        
        df5=df.copy()
        df5=df5[df5.Source!='Created by user']
        df3=df5[['Source','Supplier']].groupby(by=['Supplier']).nunique()
        df3=df3.loc[df3['Source']==1]
        df3.reset_index(inplace=True)
        for i in df3['Supplier']:
            index_names=df5[df5['Supplier']==i].index
            df5.drop(index_names,inplace=True)
        if not df5.empty:
            df5['Reason']='Vendor used both Interpreted and Supplier Portal'
            df1=df1.append(df5,ignore_index=True)
        
        df5=df[df['Current approver'].isna()==True]
        if not df5.empty:
            df6=df5.copy()
            df6['Reason']='No Current approver'
            df5=df6[df6['Document approved by'].isna()==True]
            df1=df1.append(df5,ignore_index=True)
            
        
        df2=df[df['Current approver'].isna()==False]
        a=[]
        b=[]
        for i in df2.index:
            if ',' in df2.at[i,'Current approver']:
                a.append(i)
            else:
                b.append(i)
        df5=pd.DataFrame(columns=df2.columns)
        if len(b)>0:
            for i in b:
                df5.loc[i]=df2.loc[i]
        
        excel=pd.ExcelFile("Master Data.xlsx")
        df4=excel.parse(sheet_name= 'DeptInfo')
        head_names = []
        df4['Dept head Name'].fillna('NA',inplace=True)
        for i in df4['Dept head Name']:
            name=i.split(";")
            head_names=head_names+name   
        c=[]
        for i in head_names:
            c.append(i.strip())
        
        a=[]
        b=[]
        for i in df5.index:
            if df5.at[i,'Current approver'] in c:
                a.append(i)
            else:
                b.append(i)
        df5=pd.DataFrame(columns=df.columns)
        if len(b)>0:
            for i in b:
                df5.loc[i]=df.loc[i]
            df5['Reason']='Invoice has single approver'
            df1=df1.append(df5,ignore_index=True)
        
        b=[]
        for i in df.index:
            if df.at[i,'Supplier bank account']!=df.at[i,'Interpreted bank account']:
                b.append(i)
        if len(b)>0:
            df2=pd.DataFrame(columns=df.columns)
            for i in range(len(b)):
                j=b[i]
                df2.loc[i]=df.loc[j]
            df2['Reason']='Bank Account and Interpreted Bank Account not Matching'
            df1=df1.append(df2,ignore_index=True)
        df4=pd.DataFrame(columns=df.columns)
        df5=pd.DataFrame(columns=df.columns)
        if len(b)>0:
            df3=df2[df2['Source']=='Supplier portal'].copy()
            df4=df3[['Supplier id','Supplier']].copy()
            df3=df2[df2['Source']=='Interpreted'].copy()
            df5=df3[['Supplier id','Supplier','Senders e-mail address']].copy()
        
        if not df1.empty:
            if not df4.empty:
                if not df5.empty:
                    with pd.ExcelWriter('Report_Outlier.xlsx') as writer:
                        df1.to_excel(writer, sheet_name='Outlier')
                        df4.to_excel(writer, sheet_name='Bank Mismatch-Supplier Portal')
                        df5.to_excel(writer, sheet_name='Bank Mismatch-Capture')
            if not df4.empty:
                if df5.empty:
                    with pd.ExcelWriter('Report_Outlier.xlsx') as writer:
                        df1.to_excel(writer, sheet_name='Outlier')
                        df4.to_excel(writer, sheet_name='Bank Mismatch-Supplier Portal')
            if df4.empty:
                if not df5.empty:
                    with pd.ExcelWriter('Report_Outlier.xlsx') as writer:
                        df1.to_excel(writer, sheet_name='Outlier')
                        df5.to_excel(writer, sheet_name='Bank Mismatch-Capture')
        
            if df4.empty:
                if df5.empty:
                    with pd.ExcelWriter('Report_Outlier.xlsx') as writer:
                        df1.to_excel(writer, sheet_name='Outlier')
        
        # reading files from master data
        excel=pd.ExcelFile("Master Data.xlsx")
        df6=excel.parse(sheet_name="Sheet1")
        df5=excel.parse(sheet_name ="VesselList")
        
        df1=df6[['Currency','Exchange Rate']].copy()
        df2=df6[['Company Code','Company Name','Country']].copy()
        df3=df6[['Department code','Department Name','Department Type']].copy()
        
        # Adding column for Approval Month
        for j in range(len(df['Approval start date'])):
            if df.at[j,'Approval start date'].month==1:
                df.at[j,'Approval_Month']='January'
            elif df.at[j,'Approval start date'].month==2:
                df.at[j,'Approval_Month']='February'
            elif df.at[j,'Approval start date'].month==3:
                df.at[j,'Approval_Month']='March'
            elif df.at[j,'Approval start date'].month==4:
                df.at[j,'Approval_Month']='April'
            elif df.at[j,'Approval start date'].month==5:
                df.at[j,'Approval_Month']='May'
            elif df.at[j,'Approval start date'].month==6:
                df.at[j,'Approval_Month']='June'
            elif df.at[j,'Approval start date'].month==7:
                df.at[j,'Approval_Month']='July'
            elif df.at[j,'Approval start date'].month==8:
                df.at[j,'Approval_Month']='August'
            elif df.at[j,'Approval start date'].month==9:
                df.at[j,'Approval_Month']='September'
            elif df.at[j,'Approval start date'].month==10:
                df.at[j,'Approval_Month']='October'
            elif df.at[j,'Approval start date'].month==11:
                df.at[j,'Approval_Month']='November'
            elif df.at[j,'Approval start date'].month==12:
                df.at[j,'Approval_Month']='December'
        
        # Adding column for Approval Year
        a,b=df.shape
        df.insert(b,"Approval Year",'0')
        for i in range(len(df['Approval start date'])):
            df.at[i,'Approval Year']=math.trunc(df.at[i,'Approval start date'].year)
        
        #Getting the Data Extraction Timestamp from Master Data to figure out Ageing Days
        a=df6.at[0,'Data Extraction Timestamp']
        df['Report Data Input Date']=a
        df['Report Data Input Date']=pd.to_datetime(df['Report Data Input Date'],unit='s')
        df['Time']=df['Report Data Input Date']-df['Approval start date']
        df['No of days']=df['Time'].dt.days+1
        
        #Checking if No. of Days is updating Correctly, and if data extraction timestamp is correct
        df7=df[df['No of days']<0].copy()
        if not df7.empty:
            with pd.ExcelWriter('Report_Ageing_Outlier_Ageing_Days.xlsx') as writer:
                df7.to_excel(writer, sheet_name='Sheet 1')
        
        
        if df7.empty:
            #Using No of Days, figure out Ageing Bracket
            for i in range(len(df['No of days'])):
                if df.at[i,'No of days']<=30:
                    df.at[i,'Ageing Period']='<= 30 Days'
                elif df.at[i,'No of days']<=60:
                    df.at[i,'Ageing Period']='31-60 Days'
                elif df.at[i,'No of days']<=90:
                    df.at[i,'Ageing Period']='61-90 Days'
                elif df.at[i,'No of days']<=120:
                    df.at[i,'Ageing Period']='91-120 Days'
                else:
                    df.at[i,'Ageing Period']='above 120 Days'
            
            # Correcting Vessel code
            df7=df6[['Original Vessel ID','New Vessel ID']].copy()
            df7.dropna(subset=['Original Vessel ID'],inplace=True)
            for i in df.index:
                for j in df7.index:
                    if df.at[i,'Vessel code']==df7.at[j,'Original Vessel ID']:
                        df.at[i,'Vessel code']=df7.at[j,'New Vessel ID']
            
            # handling data in df1(Currency Data),df2(Company and Location Data),df3(Department Data)
            df1.dropna(subset=["Currency"],inplace=True)
            df1=df1.reset_index()
            df1.drop(labels=['index'],axis=1,inplace=True)
            df2.dropna(subset=['Company Name'],inplace=True)
            df2=df2.reset_index()
            df2.drop(labels=['index'],axis=1,inplace=True)
            df3.dropna(subset=['Department code'],inplace=True)
            df3=df3.reset_index()
            df3.drop(labels=['index'],axis=1,inplace=True)
            
            #Changing int department codes to string
            for i in df3.index:
                if type(df3.at[i,'Department code'])==int:
                    df3.at[i,'Department code']=str(df3.at[i,'Department code'])
            
            df7=excel.parse(sheet_name = 'DeptInfo')
            df7=df7[['Dept code','Dept Head Email id','Dept ID']].copy()
            
            df=pd.merge(df,df7,left_on=['Department'],right_on=['Dept code'],how='left')
            
            # importing 'Exchange Rate' and getting 'USD Value' column using exchange rate file
            df=pd.merge(df,df1,on=['Currency'],how='left')
            df['USD Value']=df['Gross amount']/df['Exchange Rate']
            df['USD Value']=round(df['USD Value'],2)
            
            # getting columns for 'Company Code' and 'Country' 
            df=pd.merge(df,df2,left_on=['Company'],right_on=['Company Name'],how='left')
            
            # getting columns for 'Department Name' and 'Department Type'
            df=pd.merge(df,df3,left_on=['Department'],right_on=['Department code'],how='left')
            
            #Checking and correcting Vessel code line
            if df['Vessel code line'].isna().sum()>0:
                df2=df[df['Vessel code line'].isna()==True]
                a=df2.index
                for i in a:
                    df.at[i,'Vessel code line']=0
            
            # getting empty vessle codes using vessle code line
            for i in df.index:
                a=df.at[i,'Vessel code line']
                if type(a)!=float and type(a)!=int:
                    df.at[i,'Vessel Code 2']=a[0:4]
                else:
                    df.at[i,'Vessel Code 2']=a
            for i in range(len(df['Vessel Code 2'])):
                df.at[i,'Vessel Code 2']=float(df.at[i,'Vessel Code 2'])
            a=df['Vessel Code 2'].isna()
            for i in df.index:
                if a[i]==True:
                    df.at[i,'Vessel Code 2']=0
            for i in df.index:
                if df.at[i,'Vessel code']==0:
                    df.at[i,'Vessel code']=df.at[i,'Vessel Code 2']
            
            # getting column for 'New Department Grouping' which gives vessel type, using df5; also here '4 codes' corresponds to the 'Vessel code'
            df5=df5[['4 Codes','Handover Date','New Department Grouping']].copy()
            df5.dropna(subset=['4 Codes'],inplace=True)
            df5=df5.reset_index()
            df5.drop(labels=['index'],axis=1,inplace=True)
            df=pd.merge(df,df5,left_on=['Vessel code'],right_on=['4 Codes'],how='left')
            
            # defining technical and non-technical for vessles
            for i in range(len(df['Department Type'])):
                if df.at[i,'Department Type']=='NTECH':
                    df.at[i,'New Department Grouping']='Non-Technical'
            # if no vessle number given defining as 'Non-Technical'
            for i in df.index:
                if df.at[i,'Vessel code']==0:
                    df.at[i,'New Department Grouping']='Non-Technical'
                if np.isnan(df.at[i,'Vessel code']):
                    df.at[i,'New Department Grouping']='Non-Technical'
            
            df.drop(labels=['Company Name','Department code','4 Codes','Time'],axis=1,inplace=True)
            
            # Now we work on dropping repeating invoices
            # First we will figure out which vendors have repeating invoices (it is not uncommon for 2 different vendors to have the same invoice number, therefore we will only drop invoices if they come from the same vendor, as specified by the finance department)
            # For this we will use multiple versions of the groupby function 
            df2=df[['Supplier id','Serial number']].copy()
            df3=df2.groupby(by=['Supplier id']).nunique()
            df4=df2.groupby(by=['Supplier id']).count()
            df4['diff']=df4['Serial number']-df3['Serial number']
            df5=df4[df4['diff']!=0]
            # df5 now has all vendors who have repeating invoices
            
            # now running a for loop to drop repeating invoices where each loop iterates on the supplier id, all duplicate indexes are found and then dropped
            for i in df5.index:
                df6=df[df['Supplier id']==i]
                df8=df6[df6.duplicated(subset=['Serial number'],keep='first')==True]
                index_names=df8.index
                df.drop(index_names,inplace=True)
            
            # Resetting index and making sure the dataframe's index has the name index; in case this is false, the pivot table code fails, therefore this step is crucial and should not be removed
            df=df.reset_index()
            df.drop(labels=['index'],axis=1,inplace=True)
            df=df.reset_index()
            df=df.set_index('index')
            
            #checking if vessel master not updated
            if df['New Department Grouping'].isna().sum()>0:
                df2=df[df['New Department Grouping'].isna()==True]
                with pd.ExcelWriter('Report_Ageing_Outlier_Vessel_Master_Update_Needed.xlsx') as writer:
                    df2.to_excel(writer, sheet_name='Sheet 1')
            
            #Now defining outlier, which is Ageing Days>61
            df2=df[df['No of days']>=61].copy()
            
            # Checking if Input File exists. If it does, adding it to the current dataframe if it does exist
            if path.exists('Report_Ageing_Input.xlsx')==True:
                excel2=pd.ExcelFile('Report_Ageing_Input.xlsx')
                df7=excel2.parse(sheet_name='Sheet 1')
                df=df.append(df7,ignore_index=True)
                df.drop(labels=['index'],axis=1,inplace=True)
                
                # Now within this condition checking if invoices repeat and if they do showing it as outlier
                df8=df[['Supplier id','Serial number']].copy()
                df3=df8.groupby(by=['Supplier id']).nunique()
                df4=df8.groupby(by=['Supplier id']).count()
                df4['diff']=df4['Serial number']-df3['Serial number']
                df5=df4[df4['diff']!=0]
                df8=pd.DataFrame(columns=df.columns)
                index_names=[]
                for i in df5.index:
                    df6=df[df['Supplier id']==i]
                    df8=df6[df6.duplicated(subset=['Serial number'])==True]
                    index_names.append(df8.index)
                for i in range(len(index_names)):
                    j=index_names[i]
                    df5=df.loc[j]
                    df8=df8.append(df5,ignore_index=True)
                if df8.empty==False:
                    with pd.ExcelWriter('Report_Ageing_Outlier_RepeatOverMonths.xlsx') as writer:
                            df8.to_excel(writer, sheet_name='Sheet 1')
            
            # Resetting index and making sure the dataframe's index has the name index; in case this is false, the pivot table code fails, therefore this step is crucial and should not be removed
            df=df.reset_index()
            df=df.set_index('index')
            
            # Saving the datframe in an excel with the name Report1_1,2,3
            with pd.ExcelWriter('Report_Ageing_Output.xlsx') as writer:
                df.to_excel(writer, sheet_name='Sheet 1')
            with pd.ExcelWriter('Report_Ageing_Input.xlsx') as writer:
                df.to_excel(writer, sheet_name='Sheet 1')
                
            df3=df[(df['Dept Head Email id'].isna()==True) | (df['Dept ID'].isna()==True)]
            if not df3.empty:
                df4=df3[['Company','Department']].copy()
                df4.drop_duplicates(subset=['Department'], keep='first', inplace=True)
                with pd.ExcelWriter('Report_Ageing_Outlier_Email_Missing.xlsx') as writer:
                    df4.to_excel(writer, sheet_name='Sheet 1')
            
            if not df2.empty:
                
                question=input("Do you want to create an email for the outliers:(y/n):")
                
                df=df2[(df2['Dept Head Email id'].isna()==False) & (df2['Dept ID'].isna()==False)]
                df5=df2[(df2['Dept Head Email id'].isna()==True) | (df2['Dept ID'].isna()==True)]
                df2=df.copy()
                e=df2.index[0]
                d=df2.at[e,'Report Data Input Date']
                df3=df2.groupby(by=['Department']).count()
                
                if question=='y':
                    for i in df3.index:
                        df4=df2[df2['Department']==i]
                        if type(i)==int:
                            j=str(i)
                        else:
                            j=i
                        a='Report_OverAgeing_'+ j +'.xlsx'
                        with pd.ExcelWriter(a) as writer:
                            df4.to_excel(writer, sheet_name='Sheet 1')
                        e=df4.index[0]
                        f=df4.at[e,'Dept Head Email id']
                        g=df4.at[e,'Dept ID']
                        h= f+';'+g
                        excel_path=pathlib.Path(a)
                        excel_absolute=str(excel_path.absolute())
                        email(h, d, j,excel_absolute)
                if question=='n':
                    for i in df3.index:
                        df4=df2[df2['Department']==i]
                        if type(i)==int:
                            j=str(i)
                        else:
                            j=i
                        a='Report_OverAgeing_'+ j +'.xlsx'
                        with pd.ExcelWriter(a) as writer:
                            df4.to_excel(writer, sheet_name='Sheet 1')
                df2=df5.copy()
                df3=df2.groupby(by=['Department']).count()
                for i in df3.index:
                    df4=df2[df2['Department']==i]
                    if type(i)==int:
                        j=str(i)
                    else:
                        j=i
                    a='Report_OverAgeing_'+ j +'.xlsx'
                    with pd.ExcelWriter(a) as writer:
                        df4.to_excel(writer, sheet_name='Sheet 1')
            

# now we stat work on creating the pivot tables
def pivot_table(wb: object, ws1: object, pt_ws: object, ws_name: str, pt_name: str, pt_rows: list, pt_cols: list, pt_filters: list, pt_fields: list, title: str, comment: str, row_label: str, col_label: str):
    """
    wb = workbook1 reference
    ws1 = worksheet1
    pt_ws = pivot table worksheet number
    ws_name = pivot table worksheet name
    pt_name = name given to pivot table
    pt_rows, pt_cols, pt_filters, pt_fields: values selected for filling the pivot tables
    """

    # pivot table location
    pt_loc = len(pt_filters) + 5 #if you need more comments increase the value here
    
    # grab the pivot table source data
    pc = wb.PivotCaches().Create(SourceType=win32c.xlDatabase, SourceData=ws1.UsedRange)
    
    # create the pivot table object
    pc.CreatePivotTable(TableDestination=f'{ws_name}!R{pt_loc}C1', TableName=pt_name)

    # selecte the pivot table work sheet and location to create the pivot table
    pt_ws.Select()
    pt_ws.Cells(pt_loc, 1).Select()
    
    # setting format for worksheet
    pt_ws.Cells.Font.Name = "Arial"
    pt_ws.Cells.Font.Size = 9
    
    # pivot table title and comments
    pt_ws.Cells(1,1).Value = title # main title
    pt_ws.Cells(1,1).Font.Size = 12
    pt_ws.Cells(1,1).Font.Bold = True
    pt_ws.Cells(2,1).Value = comment # title which explain following pivot
    pt_ws.Cells(2,1).Font.Size = 10
    
    # Sets the rows, columns and filters of the pivot table
    for field_list, field_r in ((pt_filters, win32c.xlPageField), (pt_rows, win32c.xlRowField), (pt_cols, win32c.xlColumnField)):
        for i, value in enumerate(field_list):
            pt_ws.PivotTables(pt_name).PivotFields(value).Orientation = field_r
            pt_ws.PivotTables(pt_name).PivotFields(value).Position = i + 1
    
    # Sets the Values of the pivot table
    for field in pt_fields:
        pt_ws.PivotTables(pt_name).AddDataField(pt_ws.PivotTables(pt_name).PivotFields(field[0]), field[1], field[2]).NumberFormat = field[3]
    
    # Adding Row and Column Labels
    x=len(pt_cols)
    y=len(pt_fields)
    if y>1:
        z=2
    else:
        z=1
    pt_ws.Cells(pt_loc+x+z-1,1).Value = row_label
    pt_ws.Cells(pt_loc,2).Value = col_label
    
    # Using Pivot Table Functions to improve the Pivot Table
    pt_ws.PivotTables(pt_name).ShowValuesRow = True
    pt_ws.PivotTables(pt_name).ColumnGrand = True 
    pt_ws.PivotTables(pt_name).ShowTableStyleRowHeaders = True
    pt_ws.PivotTables(pt_name).ShowTableStyleColumnHeaders = True
    pt_ws.PivotTables(pt_name).RowGrand = True 
    pt_ws.PivotTables(pt_name).PrintTitles = True
    pt_ws.PivotTables(pt_name).TableStyle2 = "PivotStyleLight15"
    
#this function will be used to run the excel files
def run_excel(f_path: Path, f_name: str, sheet_name: str, new_sheet_name: str, pt_name: str, pt_rows: list, pt_cols: list, pt_filters: list, pt_fields: list,title: str, comment: str, row_label: str, col_label: str):

    filename = f_path / f_name

    # create excel object
    excel = client.gencache.EnsureDispatch('Excel.Application')

    # excel can be visible or not
    excel.Visible = True  # False
    
    # try except for file / path
    try:
        wb = excel.Workbooks.Open(filename)
    except com_error as e:
        if e.excepinfo[5] == -2146827284:
            print(f'Failed to open spreadsheet.  Invalid filename or location: {filename}')
        else:
            raise e
        sys.exit(1)

    # set worksheet
    ws1 = wb.Sheets(sheet_name)
    
    # Setup and call pivot_table
    ws2_name = new_sheet_name
    wb.Sheets.Add().Name = ws2_name
    ws2 = wb.Sheets(ws2_name)
    
    
    # Calling Pivot Table function
    pivot_table(wb, ws1, ws2, ws2_name, pt_name, pt_rows, pt_cols, pt_filters, pt_fields, title, comment, row_label, col_label)
    
    #save the excel file
    wb.Save()
    
#Following is the Code for every example of the Pivot Table with all Parameters
#If you need to change parameters change here. If you need more pivot tables define another function and change parameters accordingly.

def pt1(f_path: Path, f_name: str, sheet_name: str):
    
    #Parameters for each iteration
    name_of_new_sheet='SheetPT'  #need to change in every iteration
    pt_name = 'Table01'  # must be a string
    pt_rows = ['Country','Department','Department Name']  # must be a list
    pt_cols = ['Ageing Period']  # must be a list
    pt_filters = ['Company','Country','New Department Grouping']  # must be a list
    # [0]: field name [1]: pivot table column name [3]: calulation method [4]: number format
    pt_fields = [ ['Invoice no', 'No of Invoices', win32c.xlCount, '0'],
                 ['USD Value', '$ Amount', win32c.xlSum, '#,##0.00']] # must be list of lists
    Title  = 'Fleet Group of Companies' # must be string
    Comment = 'Ageing Report on Department Basis' # must be string
    row_label = 'Department' # must be string
    column_label = 'Ageing Bracket' # must be string
    run_excel(f_path,f_name, sheet_name,name_of_new_sheet, pt_name, pt_rows, pt_cols, pt_filters, pt_fields, Title, Comment, row_label, column_label)

def write():
    
    # changing file path and name
    f_path=Path.cwd()
    f_name='Report_Ageing_Output.xlsx'
    sheet_name='Sheet 1'
    
    # calling other functions
    pt1(f_path, f_name, sheet_name)

working()
if os.path.exists('Report_Ageing_Output.xlsx')==True:
    write()