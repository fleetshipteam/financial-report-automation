# -*- coding: utf-8 -*-
"""
Created on Tue Aug 24 16:53:13 2021

@author: kunal.s.chauhan
"""

import pandas as pd
import numpy as np
import os
from pathlib import Path
import sys
import os.path
from os import path


#Importing required files
excel=pd.ExcelFile("ES_Data_Capture_User.xlsx")
df=excel.parse(sheet_name="Invoice")
excel=pd.ExcelFile("Master Data.xlsx")
df4=excel.parse(sheet_name="Sheet1")
df6=excel.parse(sheet_name="CaptureUser")

#Getting Country from Master Data to add in df
df3=df4[['Company Code','Company Name','Country']].copy()
df3.dropna(subset=['Company Name'],inplace=True)
df3=df3.reset_index()
df3.drop(labels=['index'],axis=1,inplace=True)
df=pd.merge(df,df3,left_on=['Company'],right_on=['Company Name'],how='left')

#Adding country to df6 to compare with df
df2=df6[['Scan id From','Scan id To','IR PIC','Country']].copy()
df2.dropna(subset=['IR PIC'],inplace=True)
for i in df2.index:
    df2.at[i,'Code']=df2.at[i,'Country'].replace(' ','')
    df2.at[i,'Country']=df2.at[i,'Code'].upper()
df3=df6[['Country Code','Location']].copy()
df3.dropna(subset=['Country Code'],inplace=True)
df2=pd.merge(df2,df3,left_on=['Country'],right_on=['Country Code'],how='left')

df2=df2.reset_index(drop=True)

#Adding accountant for column with Source: Interpreted, Location matching, Scan no within range
for i in df.index:
    if df.at[i,'Source']=='Interpreted':
        for j in df2.index:
            if (df.at[i,'Country']==df2.at[j,'Location']) and (df.at[i,'Scan no']>=df2.at[j,'Scan id From'] and df.at[i,'Scan no']<=df2.at[j,'Scan id To']):
                df.at[i,'Accountant']=df2.at[j,'IR PIC']

#Checking for any accountants that did not update to add as Outlier
df2=df[df['Accountant']=='Supplier Portal']
w=[]
for i in df2.index:
    if df2.at[i,'Source']=='Interpreted':
        w.append(i)
df5=pd.DataFrame(columns=df.columns)
if len(w)>0:
    for i in range(len(w)):
        j=w[i]
        df5.loc[i]=df.loc[j]

#If df5 is not empty, then outliers exist, if so, giving that as output
if not df5.empty:
    with pd.ExcelWriter('Report_Capture_User_Outlier.xlsx') as writer:
        df5.to_excel(writer, sheet_name='Sheet 1')

#If df5 is empty, then no outliers exist, if so, giving output
if df5.empty:
    with pd.ExcelWriter('Report_Capture_User_Output.xlsx') as writer:
        df.to_excel(writer, sheet_name='Sheet 1')
        