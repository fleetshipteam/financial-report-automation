# -*- coding: utf-8 -*-
"""
Created on Thu Aug 26 19:42:02 2021

@author: kunal.s.chauhan
"""

from datetime import date
from datetime import datetime, timedelta
import datetime as dt
import pandas as pd
import numpy as np
from datetime import date
import win32com.client as client
import os
from pywintypes import com_error
import pathlib
from pathlib import Path
import sys
import os.path
from os import path

win32c=client.constants
outlook=client.Dispatch("Outlook.Application")
excel=client.Dispatch('Excel.Application')
excel = client.gencache.EnsureDispatch('Excel.Application')

global mail_list

def email(to:str, link:str):
    d=date.today()
    message=outlook.CreateItem(0)
    message.Display()
    message.To=to
    message.CC='vineetrathi@fleetship.com; salima.martrust@fleetship.com; nachiket.martrust@fleetship.com'
    message.Attachments.Add(link)
    message.Subject="AP Invoice- Over Ageing as on "+str(d)
    html_body= """
        <div>
            </b>Hi All,</b>
        </div><br>
        <div>
            Find attached the invoices approved in Eye Share is not transferred to Oracle. <br>
            Invoices in the bracket above 2 days should have been transferred to Oracle on Friday. <br> <br>
            Note: This is a system generate email and need not be responded to.
        </div><br>
        <div>
            Thanks and regards<br>
            Accounts Payable<br>
            Fleet Management Limited<br>
        </div>
        """
    message.HTMLBody=html_body
    message.Display()
    #message.Send()

def working():
    
    #importing required ecxels
    excel=pd.ExcelFile("ES_Data_Approved_Ready_For_Transfer.xlsx")
    df=excel.parse(sheet_name = "Invoice")
    excel=pd.ExcelFile("Master Data.xlsx")
    df1=excel.parse(sheet_name = "Sheet1")
    df2=excel.parse(sheet_name = "VesselList")
    
    df['Document approved date']=pd.to_datetime(df['Document approved date'], unit='s')
    # checking if any Document Approved Date Missing
    if df['Document approved date'].isna().sum()>0:
        df2=df[df['Document approved date'].isna()==True].copy()
        with pd.ExcelWriter('Report_Approved_Ready_For_Transfer_Outlier_Document_approved_date_Missing.xlsx') as writer:
            df2.to_excel(writer, sheet_name='Sheet 1')
    else:
        a=datetime.now()
        df['Tool Execution Date']=a
        df['Time']=df['Tool Execution Date']-df['Document approved date']
        df['Ageing Days']=df['Time'].dt.days+1
        
        if 'Accountant' in df.columns:
            df.drop(labels=['Accountant'],axis=1,inplace=True)    
        
        for i in df.index:
            if df.at[i,'Ageing Days']<=1:
                df.at[i,'Ageing bracket']='0-1 days'
            elif df.at[i,'Ageing Days']<=5:
                df.at[i,'Ageing bracket']='2-5 days'
            elif df.at[i,'Ageing Days']>=6:
                df.at[i,'Ageing bracket']='6+ days'
        
        df7=df1[['Company Code','Company Name','Country']].copy()
        df7.dropna(subset=['Company Code'],inplace=True)
        df=pd.merge(df,df7,left_on=['Company'],right_on=['Company Name'],how='left')
        
        df3=df2[['4 Codes','Accountant']].copy()
        df3.dropna(subset=['Accountant'],inplace=True)
        df=pd.merge(df,df3,left_on=['Vessel code'],right_on=['4 Codes'],how='left')
        
        df4=df1[['Department','Designated Accountant']].copy()
        df4.dropna(subset=['Designated Accountant'],inplace=True)
        df=pd.merge(df,df4,on=['Department'],how='left')
        
        df6=df[df['Designated Accountant'].isna()==False].copy()
        for i in df6.index:
            df.at[i,'Accountant']=df.at[i,'Designated Accountant']
        df.drop(labels=['Designated Accountant'],axis=1,inplace=True)
        
        a=df1.at[0,'Designated_Accountant_0000']
        
        for i in df.index:
            if df.at[i,'Vessel code']==0:
                df.at[i,'Accountant']=a
            if df.at[i,'Vessel code']=='0000':
                df.at[i,'Accountant']=a
        
        df5=df1[['Current Accountant','Location','Replace By']].copy()
        df5.dropna(subset=['Replace By'],inplace=True)
        for i in df.index:
            for j in df5.index:
                if df.at[i,'Country']==df5.at[j,'Location'] and df.at[i,'Accountant']==df5.at[j,'Current Accountant']:
                    df.at[i,'Accountant']=df5.at[j,'Replace By']
        
        df8=df1[['Accountant','Accountant email id']].copy()
        df8.dropna(subset=['Accountant'],inplace=True)
        df=pd.merge(df,df8,on=['Accountant'],how='left')
        
        if df['Accountant email id'].isna().sum()>0:
            df2=df[df['Accountant email id'].isna()==True]
            with pd.ExcelWriter('Report_Approved_Ready_For_Transfer_Email_Missing.xlsx') as writer:
                df2.to_excel(writer, sheet_name='Sheet 1')
        
        df3=df[df['Accountant email id'].isna()==False]
        df3=df3.groupby(by=['Accountant email id']).sum()
        a=''
        for i in df3.index:
            if len(a)==0:
                a=i
            elif len(a)>0:
                a=a+';'+i
        global mail_list
        mail_list=a
        
        # Now we work on dropping repeating invoices
        # First we will figure out which vendors have repeating invoices (it is not uncommon for 2 different vendors to have the same invoice number, therefore we will only drop invoices if they come from the same vendor, as specified by the finance department)
        # For this we will use multiple versions of the groupby function 
        df2=df[['Supplier id','Serial number']].copy()
        df3=df2.groupby(by=['Supplier id']).nunique()
        df4=df2.groupby(by=['Supplier id']).count()
        df4['diff']=df4['Serial number']-df3['Serial number']
        df5=df4[df4['diff']!=0]
        # df5 now has all vendors who have repeating invoices
        
        # now running a for loop to drop repeating invoices where each loop iterates on the supplier id, all duplicate indexes are found and then dropped
        for i in df5.index:
            df6=df[df['Supplier id']==i]
            df8=df6[df6.duplicated(subset=['Serial number'],keep='first')==True]
            index_names=df8.index
            df.drop(index_names,inplace=True)
        
        df=df.reset_index()
        df.drop(labels=['index'],axis=1,inplace=True)
        df=df.reset_index()
        df=df.set_index('index')
        
        with pd.ExcelWriter('Report_Approved_Ready_For_Transfer.xlsx') as writer:
            df.to_excel(writer, sheet_name='Sheet 1')

# now we stat work on creating the pivot tables
def pivot_table(wb: object, ws1: object, pt_ws: object, ws_name: str, pt_name: str, pt_rows: list, pt_cols: list, pt_filters: list, pt_fields: list, title: str, comment: str, row_label: str, col_label: str):
    """
    wb = workbook1 reference
    ws1 = worksheet1
    pt_ws = pivot table worksheet number
    ws_name = pivot table worksheet name
    pt_name = name given to pivot table
    pt_rows, pt_cols, pt_filters, pt_fields: values selected for filling the pivot tables
    """

    # pivot table location
    pt_loc = len(pt_filters) + 5 #if you need more comments increase the value here
    
    # grab the pivot table source data
    pc = wb.PivotCaches().Create(SourceType=win32c.xlDatabase, SourceData=ws1.UsedRange)
    
    # create the pivot table object
    pc.CreatePivotTable(TableDestination=f'{ws_name}!R{pt_loc}C1', TableName=pt_name)

    # selecte the pivot table work sheet and location to create the pivot table
    pt_ws.Select()
    pt_ws.Cells(pt_loc, 1).Select()
    
    # setting format for worksheet
    pt_ws.Cells.Font.Name = "Arial"
    pt_ws.Cells.Font.Size = 9
    
    # pivot table title and comments
    pt_ws.Cells(1,1).Value = title # main title
    pt_ws.Cells(1,1).Font.Size = 12
    pt_ws.Cells(1,1).Font.Bold = True
    pt_ws.Cells(2,1).Value = comment # title which explain following pivot
    pt_ws.Cells(2,1).Font.Size = 10
    
    # Sets the rows, columns and filters of the pivot table
    for field_list, field_r in ((pt_filters, win32c.xlPageField), (pt_rows, win32c.xlRowField), (pt_cols, win32c.xlColumnField)):
        for i, value in enumerate(field_list):
            pt_ws.PivotTables(pt_name).PivotFields(value).Orientation = field_r
            pt_ws.PivotTables(pt_name).PivotFields(value).Position = i + 1
    
    # Sets the Values of the pivot table
    for field in pt_fields:
        pt_ws.PivotTables(pt_name).AddDataField(pt_ws.PivotTables(pt_name).PivotFields(field[0]), field[1], field[2]).NumberFormat = field[3]
    
    # Adding Row and Column Labels
    x=len(pt_cols)
    y=len(pt_fields)
    if y>1:
        z=2
    else:
        z=1
    pt_ws.Cells(pt_loc+x+z-1,1).Value = row_label
    pt_ws.Cells(pt_loc,2).Value = col_label
    
    # Using Pivot Table Functions to improve the Pivot Table
    pt_ws.PivotTables(pt_name).ShowValuesRow = True
    pt_ws.PivotTables(pt_name).ColumnGrand = True 
    pt_ws.PivotTables(pt_name).ShowTableStyleRowHeaders = True
    pt_ws.PivotTables(pt_name).ShowTableStyleColumnHeaders = True
    pt_ws.PivotTables(pt_name).RowGrand = True 
    pt_ws.PivotTables(pt_name).PrintTitles = True
    pt_ws.PivotTables(pt_name).TableStyle2 = "PivotStyleLight15"

#this function will be used to run the excel files
def run_excel(f_path: Path, f_name: str, sheet_name: str, new_sheet_name: str, pt_name: str, pt_rows: list, pt_cols: list, pt_filters: list, pt_fields: list,title: str, comment: str, row_label: str, col_label: str):

    filename = f_path / f_name

    # create excel object
    excel = client.gencache.EnsureDispatch('Excel.Application')

    # excel can be visible or not
    excel.Visible = True  # False
    
    # try except for file / path
    try:
        wb = excel.Workbooks.Open(filename)
    except com_error as e:
        if e.excepinfo[5] == -2146827284:
            print(f'Failed to open spreadsheet.  Invalid filename or location: {filename}')
        else:
            raise e
        sys.exit(1)

    # set worksheet
    ws1 = wb.Sheets(sheet_name)
    
    # Setup and call pivot_table
    ws2_name = new_sheet_name
    wb.Sheets.Add().Name = ws2_name
    ws2 = wb.Sheets(ws2_name)
    
    
    # Calling Pivot Table function
    pivot_table(wb, ws1, ws2, ws2_name, pt_name, pt_rows, pt_cols, pt_filters, pt_fields, title, comment, row_label, col_label)
    
    #save the excel file
    wb.Save()

# pivot table for report 2
def pt(f_path: Path, f_name: str, sheet_name: str):
    
    #Parameters for each iteration
    name_of_new_sheet='SheetPT1'  #need to change in every iteration
    pt_name = 'Table01'  # must be a string
    pt_rows = ['Invoice priority','Accountant']  # must be a list
    pt_cols = ['Ageing bracket']  # must be a list
    pt_filters = ['Company','Country','Invoice priority']  # must be a list
    # [0]: field name [1]: pivot table column name [3]: calulation method [4]: number format
    pt_fields = [['Serial number', 'No of Invoice', win32c.xlCount, '0']] # must be list of lists
    Title  = 'Approved and Ready For Transfer-Invoices in Eye Share' # must be string
    Comment = '' # must be string
    row_label = 'Accountant' # must be string
    column_label = 'Ageing bracket' # must be string
    run_excel(f_path,f_name, sheet_name,name_of_new_sheet, pt_name, pt_rows, pt_cols, pt_filters, pt_fields, Title, Comment, row_label, column_label)

# defining the file path, name and sheet name
def pivot():
    # changing file path and name
    f_path=Path.cwd()
    f_name='Report_Approved_Ready_For_Transfer.xlsx'
    sheet_name='Sheet 1'
    
    # calling other functions
    pt(f_path, f_name, sheet_name)

# calling functions to complete code
working()
if os.path.exists('Report_Approved_Ready_For_Transfer.xlsx')==True:
    pivot()
    excel_path=pathlib.Path('Report_Approved_Ready_For_Transfer.xlsx')
    excel_absolute=str(excel_path.absolute())
    email(mail_list,excel_absolute)